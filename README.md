# ABOUT #
This utility allows you to save paths under aliases.

# USAGE #

cdd set <name>  -- store current path by alias <name>"       
cdd [name]      -- change dir to path stored by alias [name]"
cdd list        -- list all stored dirs"                     
cdd List        -- list all stored dirs with path"           
cdd --help [-h] -- current message"                          

# INSTAL #

## MANUAL ##

1. Change "_CDD_STORE_PATH" and "_CDD_FILE_PREFIX" in cdd file
2. Add to ~/.bashrc "source <path>/cdd"

## AUTOMATIC ##

execute ./install.sh --install-dir=<dir> --store-path=<path> --file-prefix=<prefix>
