#!/bin/bash

INSTALL_DIR="$HOME/bin"
STORE_PATH="$HOME/.local/cdd"
FILE_PREFIX="cd.file."


help()
{
    echo "Usage: ./install.sh --install-dir=<dir> --store-path=<path> --file-prefix=<prefix>"
    echo ""
    echo "    --install-dir    -- set installation path of cdd utility. (Default: $INSTALL_DIR)"
    echo "    --store-path     -- set path of cdd files. (Default: $STORE_PATH)"
    echo "    --file-prefix    -- set prefix for cdd files. (Default: $FILE_PREFIX)"
    echo "    -h, --help       -- this messages."
    echo ""
}

while [[ $# -gt 0 ]]; do
    key=$1
    case $key in
        -h | --help)
            help
            exit 0
            ;;
        --install-dir)
            INSTALL_DIR="$2"
            shift; shift; ;;
        --install-dir=*)
            INSTALL_DIR="$(echo $key | cut -d "=" -f 2)"
            shift; ;;
        --store-path)
            STORE_PATH="$2"
            shift; shift; ;;
        --store-path=*)
            STORE_PATH="$(echo $key | cut -d "=" -f 2)"
            shift; ;;
        --file-prefix)
            FILE_PREFIX="$2"
            shift; shift; ;;
        --file-prefix)
            FILE_PREFIX="$(echo $key | cut -d "=" -f 2)"
            shift; ;;
        *)
            echo "Unsupported key: $key"
            echo ""
            help
            exit 0
            ;;
    esac
done

echo "Config:"
echo "    install dir: $INSTALL_DIR"
echo "    store path:  $STORE_PATH"
echo "    file prefix: $FILE_PREFIX"
echo ""

echo "Create $INSTALL_DIR path"
mkdir -p "$INSTALL_DIR"

echo "Set _CDD_STORE_PATH=\"$STORE_PATH\" and _CDD_FILE_PREFIX\"$FILE_PREFIX\" in cdd and copy to the $INSTALL_DIR"

sed -e "s@^_CDD_STORE_PATH.*@_CDD_STORE_PATH=\"$STORE_PATH\"@" \
    -e "s@^_CDD_FILE_PREFIX.*@_CDD_FILE_PREFIX=\"$FILE_PREFIX\"@" cdd > "$INSTALL_DIR/cdd"

echo "Patch $HOME/.bashrc"
grep -q '^source .*cdd["]*$' "$HOME/.bashrc"
if [ "$?" = "1" ]; then
    echo "source \"$INSTALL_DIR/cdd\"" >> "$HOME/.bashrc"
else
    sed -i 's@^source .*cdd["]*$@source "'"$INSTALL_DIR/cdd"'"@' "$HOME/.bashrc"
fi

echo "Done"
